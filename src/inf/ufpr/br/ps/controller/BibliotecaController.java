package inf.ufpr.br.ps.controller;

import inf.ufpr.br.ps.interfaces.InterfaceBiblioteca;

public class BibliotecaController {

	public static void main(String[] args) {

		int entrada = -1;
		while (entrada != 0) {
			entrada = InterfaceBiblioteca.exibeMenu();
			switch (entrada) {
			case 0:
				break;
			case 1:
				CadastroUsuarioController cadastroUsuario = new CadastroUsuarioController();
				cadastroUsuario.novoUsuario();
				break;
			case 2:
				CadastroLivroController cadastroLivro = new CadastroLivroController();
				cadastroLivro.novoLivro();
				break;
			case 3:
				CadastroPeriodicoController cadastroPeriodico = new CadastroPeriodicoController();
				cadastroPeriodico.novoPeriodico();
				break;
			case 4:
				PesquisaLivroController pesquisaLivro = new PesquisaLivroController();
				pesquisaLivro.pesquisarLivro();
				break;
			case 5:
				PesquisaPeriodicoController pesquisaPeriodico = new PesquisaPeriodicoController();
				pesquisaPeriodico.pesquisarPeriodico();
				break;
			case 6:
				EmprestaLivroController emprestaLivro = new EmprestaLivroController();
				emprestaLivro.emprestarLivro();
				break;
			case 7:
				EmprestaPeriodicoController emprestaPeriodico = new EmprestaPeriodicoController();
				emprestaPeriodico.emprestarPeriodico();
				break;
			case 8:
				DevolveObraController devolveObra = new DevolveObraController();
				devolveObra.DevolveObra();
				break;
			}
		}
	}
}