package inf.ufpr.br.ps.controller;

import inf.ufpr.br.ps.dao.LivroDAO;
import inf.ufpr.br.ps.interfaces.InterfaceCadastroLivro;
import inf.ufpr.br.ps.obras.Livro;
import inf.ufpr.br.ps.singleton.LivrosSingleton;

public class CadastroLivroController {

	public void novoLivro() {
		Livro livro = new Livro();
		LivroDAO livros = LivrosSingleton.getInstance();
		livro = InterfaceCadastroLivro.exibirTela();
		livros.addLivro(livro);
		InterfaceCadastroLivro.exibirSucesso();
	}
}
