package inf.ufpr.br.ps.controller;

import inf.ufpr.br.ps.dao.PeriodicoDAO;
import inf.ufpr.br.ps.interfaces.InterfaceCadastroPeriodico;
import inf.ufpr.br.ps.obras.Periodico;
import inf.ufpr.br.ps.singleton.PeriodicosSingleton;

public class CadastroPeriodicoController {

	public void novoPeriodico() {
		Periodico periodico = new Periodico();
		PeriodicoDAO periodicos = PeriodicosSingleton.getInstance();
		periodico = InterfaceCadastroPeriodico.exibirTela();
		periodicos.addPeriodico(periodico);
		InterfaceCadastroPeriodico.exibirSucesso();
	}
}
