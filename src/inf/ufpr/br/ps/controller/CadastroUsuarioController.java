package inf.ufpr.br.ps.controller;

import inf.ufpr.br.ps.dao.UsuarioDAO;
import inf.ufpr.br.ps.interfaces.InterfaceCadastroUsuario;
import inf.ufpr.br.ps.singleton.UsuariosSingleton;
import inf.ufpr.br.ps.usuarios.Aluno;
import inf.ufpr.br.ps.usuarios.Geral;
import inf.ufpr.br.ps.usuarios.Professor;

public class CadastroUsuarioController {

	public void novoUsuario() {
	    UsuarioDAO usuarios = UsuariosSingleton.getInstance();
		int tipoUser;
		tipoUser = InterfaceCadastroUsuario.exibirTela();
		switch (tipoUser) {
		case 1:
			Professor professor = new Professor();
			professor = InterfaceCadastroUsuario.exibirTelaProfessor();
			usuarios.addUsuario(professor);
			break;
		case 2:
			Aluno aluno = new Aluno();
			aluno = InterfaceCadastroUsuario.exibirTelaAluno();
			usuarios.addUsuario(aluno);
			break;
		case 3:
			Geral geral = new Geral();
			geral = InterfaceCadastroUsuario.exibirTelaGeral();
			usuarios.addUsuario(geral);
			break;
		}
		InterfaceCadastroUsuario.exibirSucesso();
	}
}
