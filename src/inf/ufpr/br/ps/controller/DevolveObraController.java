package inf.ufpr.br.ps.controller;

import inf.ufpr.br.ps.dao.UsuarioDAO;
import inf.ufpr.br.ps.interfaces.InterfaceDevolveObra;
import inf.ufpr.br.ps.singleton.UsuariosSingleton;
import inf.ufpr.br.ps.usuarios.Usuario;

public class DevolveObraController {
	public void DevolveObra() {
		Usuario usuario = new Usuario();
		UsuarioDAO usuarios = UsuariosSingleton.getInstance();
		String cpf = InterfaceDevolveObra.exibirTelaUsuario();
		usuario = usuarios.buscaUsuario(cpf);
		int pendendencias = InterfaceDevolveObra.exibirPendencias(usuario);
		if (pendendencias != 0) {
			int obra = InterfaceDevolveObra.selecionaObra();
			String dataDevolucao = InterfaceDevolveObra.dataDevolucao();
			usuario.getEmprestimos().get(obra).setDataDevolucao(dataDevolucao);
			if (usuario.getEmprestimos().get(obra).calculaMulta() > 0) {
				int opc = InterfaceDevolveObra.pagarMulta(usuario
						.getEmprestimos().get(obra).calculaMulta());
				if (opc != 0) {
					InterfaceDevolveObra.logMultaNaoPaga();
					InterfaceDevolveObra.logDevolucaoNaoFeita();
				} else {
					InterfaceDevolveObra.logMultaSucesso();
					usuario.getEmprestimos()
							.get(obra)
							.getObra()
							.setExemplaresDisponiveis(
									usuario.getEmprestimos().get(obra)
											.getObra()
											.getExemplaresDisponiveis() + 1);
					usuario.getEmprestimos().remove(obra);
					InterfaceDevolveObra.logDevolucaoSucesso();
				}
			} else {
//				InterfaceDevolveObra.logMultaSucesso();
				usuario.getEmprestimos()
						.get(obra)
						.getObra()
						.setExemplaresDisponiveis(
								usuario.getEmprestimos().get(obra).getObra()
										.getExemplaresDisponiveis() + 1);
				usuario.getEmprestimos().remove(obra);
				InterfaceDevolveObra.logDevolucaoSucesso();
			}
		}
	}
}
