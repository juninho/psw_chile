package inf.ufpr.br.ps.controller;

import inf.ufpr.br.ps.dao.LivroDAO;
import inf.ufpr.br.ps.dao.UsuarioDAO;
import inf.ufpr.br.ps.interfaces.InterfaceEmprestaLivro;
import inf.ufpr.br.ps.obras.Livro;
import inf.ufpr.br.ps.singleton.LivrosSingleton;
import inf.ufpr.br.ps.singleton.UsuariosSingleton;
import inf.ufpr.br.ps.usuarios.Emprestimo;
import inf.ufpr.br.ps.usuarios.Usuario;

public class EmprestaLivroController {

	public void emprestarLivro() {
		Emprestimo emprestimo = new Emprestimo();
		Livro livro = new Livro();
		Usuario usuario = new Usuario();
		LivroDAO livros = LivrosSingleton.getInstance();
		UsuarioDAO usuarios = UsuariosSingleton.getInstance();

		String nome = InterfaceEmprestaLivro.exibirTelaLivro();
		livro = livros.buscaLivro(nome);
		if (livro.getExemplaresDisponiveis() > 0) {
			emprestimo.setObra(livro);
			String cpf = InterfaceEmprestaLivro.exibirTelaUsuario();
			usuario = usuarios.buscaUsuario(cpf);
			if (usuario.getMaxEmp() != usuario.getEmprestimos().size()) {
				String dataEmprestimo = InterfaceEmprestaLivro.exibirTelaData();
				emprestimo.setDataEmprestimo(dataEmprestimo,
						usuario.getMaxDias());
				livro.setExemplaresDisponiveis(livro.getExemplaresDisponiveis() - 1);
				usuario.novoEmprestimo(emprestimo);
				InterfaceEmprestaLivro.exibirTelaSucesso(emprestimo.getDataLimite());
			} else {
				InterfaceEmprestaLivro.exibirTelaLimiteEmprestimos();
			}
		} else {
			InterfaceEmprestaLivro.exibirTelaSemLivros();
		}
	}
}
