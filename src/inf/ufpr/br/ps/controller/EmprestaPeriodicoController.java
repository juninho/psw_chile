package inf.ufpr.br.ps.controller;

import inf.ufpr.br.ps.dao.PeriodicoDAO;
import inf.ufpr.br.ps.dao.UsuarioDAO;
import inf.ufpr.br.ps.interfaces.InterfaceEmprestaPeriodico;
import inf.ufpr.br.ps.obras.Periodico;
import inf.ufpr.br.ps.singleton.PeriodicosSingleton;
import inf.ufpr.br.ps.singleton.UsuariosSingleton;
import inf.ufpr.br.ps.usuarios.Emprestimo;
import inf.ufpr.br.ps.usuarios.Usuario;

public class EmprestaPeriodicoController {
	public void emprestarPeriodico() {
		Emprestimo emprestimo = new Emprestimo();
		Periodico periodico = new Periodico();
		Usuario usuario = new Usuario();
		PeriodicoDAO periodicos = PeriodicosSingleton.getInstance();
		UsuarioDAO usuarios = UsuariosSingleton.getInstance();

		String nome = InterfaceEmprestaPeriodico.exibirTelaPeriodico();

		periodico = periodicos.buscaPeriodico(nome);
		if (periodico.getExemplaresDisponiveis() > 0) {
			emprestimo.setObra(periodico);
			String cpf = InterfaceEmprestaPeriodico.exibirTelaUsuario();
			usuario = usuarios.buscaUsuario(cpf);
			if (usuario.getMaxEmp() != usuario.getEmprestimos().size()) {
				String dataEmprestimo = InterfaceEmprestaPeriodico.exibirTelaData();
				emprestimo.setDataEmprestimo(dataEmprestimo,
						usuario.getMaxDias());
				periodico.setExemplaresDisponiveis(periodico
						.getExemplaresDisponiveis() - 1);
				usuario.novoEmprestimo(emprestimo);
				InterfaceEmprestaPeriodico.exibirTelaSucesso(emprestimo.getDataLimite());
			} else {
				InterfaceEmprestaPeriodico.exibirTelaLimiteEmprestimos();
			}
		} else {
			InterfaceEmprestaPeriodico.exibirTelaSemLivros();
		}
	}
}
