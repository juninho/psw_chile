package inf.ufpr.br.ps.controller;

import inf.ufpr.br.ps.dao.LivroDAO;
import inf.ufpr.br.ps.interfaces.InterfacePesquisaLivro;
import inf.ufpr.br.ps.obras.Livro;
import inf.ufpr.br.ps.singleton.LivrosSingleton;

public class PesquisaLivroController {

	public void pesquisarLivro() {
		int controle = 0;
		while (controle != 2) {
			Livro livro = new Livro();
			String nome;
			LivroDAO livros = LivrosSingleton.getInstance();
			nome = InterfacePesquisaLivro.exibirTela();
			livro = livros.buscaLivro(nome);
			
			if (livro.equals(null)) {
				controle = InterfacePesquisaLivro.exibirTelaNaoEncontrado();
			} else {
				InterfacePesquisaLivro.exibirLivro(livro);
				controle = 2;
			}
		}
	}
}
