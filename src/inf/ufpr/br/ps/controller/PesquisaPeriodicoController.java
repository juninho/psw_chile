package inf.ufpr.br.ps.controller;

import inf.ufpr.br.ps.dao.PeriodicoDAO;
import inf.ufpr.br.ps.interfaces.InterfacePesquisaPeriodico;
import inf.ufpr.br.ps.obras.Periodico;
import inf.ufpr.br.ps.singleton.PeriodicosSingleton;

public class PesquisaPeriodicoController {
	public void pesquisarPeriodico() {
		int controle = 0;
		while (controle != 2) {
			Periodico periodico = new Periodico();
			String nome;
			PeriodicoDAO periodicos = PeriodicosSingleton.getInstance();
			nome = InterfacePesquisaPeriodico.exibirTela();
			periodico = periodicos.buscaPeriodico(nome);
			
			if (periodico.equals(null)) {
				controle = InterfacePesquisaPeriodico.exibirTelaNaoEncontrado();
			} else {
				InterfacePesquisaPeriodico.exibirPeriodico(periodico);
				controle = 2;
			}
		}
	}
	
}
