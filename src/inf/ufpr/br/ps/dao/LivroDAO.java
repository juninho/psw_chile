package inf.ufpr.br.ps.dao;

import inf.ufpr.br.ps.obras.Livro;

import java.util.ArrayList;
import java.util.List;

public class LivroDAO {
	List<Livro> livros = new ArrayList<Livro>();

	public List<Livro> getLivros() {
		return livros;
	}

	public void addLivro(Livro livro) {
		this.livros.add(livro);
	}

	public Livro buscaLivro(String nomeLivro) {
		for (int i = 0; i < this.livros.size(); i++) {
			if (this.livros.get(i).getNome().equals(nomeLivro)) {
				return this.livros.get(i);
			}
		}
		return null;
	}
}
