package inf.ufpr.br.ps.dao;

import inf.ufpr.br.ps.obras.Periodico;

import java.util.ArrayList;
import java.util.List;

public class PeriodicoDAO {
	List<Periodico> periodicos = new ArrayList<Periodico>();

	public void addPeriodico(Periodico periodico) {
		this.periodicos.add(periodico);
	}

	public Periodico buscaPeriodico(String nomePeriodico) {
		for (int i = 0; i < this.periodicos.size(); i++) {
			if (this.periodicos.get(i).getNome().equals(nomePeriodico)) {
				return this.periodicos.get(i);
			}
		}
		return null;
	}
}
