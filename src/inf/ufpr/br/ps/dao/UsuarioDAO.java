package inf.ufpr.br.ps.dao;

import inf.ufpr.br.ps.usuarios.Usuario;

import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO {
	List<Usuario> usuarios = new ArrayList<Usuario>();

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void addUsuario(Usuario usuario) {
		this.usuarios.add(usuario);
	}

	public Usuario buscaUsuario(String cpf) {
		for (int i = 0; i < this.usuarios.size(); i++)
			if (this.usuarios.get(i).getCpf().equals(cpf)) {
				return this.usuarios.get(i);
			}
		return null;
	}
}
