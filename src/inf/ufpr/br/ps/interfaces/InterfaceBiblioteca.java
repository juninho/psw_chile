package inf.ufpr.br.ps.interfaces;

import java.util.Scanner;

public class InterfaceBiblioteca {
	public static int exibeMenu() {
		int entrada;
		Scanner ler = new Scanner(System.in);
		System.out
				.println("########################## SISTEMA BIBLIOTECA ##########################");
		System.out
				.println("#### Digite o numero da operacao que deseja realizar:               ####");
		System.out
				.println("#### 1- Cadastrar Usuario                                           ####");
		System.out
				.println("#### 2- Cadastrar Livro                                             ####");
		System.out
				.println("#### 3- Cadastrar Periodico                                         ####");
		System.out
				.println("#### 4- Pesquisar Livro                                             ####");
		System.out
				.println("#### 5- Pesquisar Periodico                                         ####");
		System.out
				.println("#### 6- Emprestar Livro                                             ####");
		System.out
				.println("#### 7- Emprestar Periodico                                         ####");
		System.out
				.println("#### 8- Devolver Obra                                               ####");
		System.out
				.println("#### 0- Sair                                                        ####");
		System.out
				.println("########################################################################");
		entrada = ler.nextInt();
		return entrada;
	}
}
