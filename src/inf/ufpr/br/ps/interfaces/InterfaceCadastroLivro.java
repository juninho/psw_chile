package inf.ufpr.br.ps.interfaces;

import inf.ufpr.br.ps.obras.Livro;

import java.util.Scanner;

public class InterfaceCadastroLivro {
	public static Livro exibirTela() {
		Scanner ler = new Scanner(System.in);
		Livro livro = new Livro();

		System.out.println("############## CADASTRO LIVRO #################");
		System.out.println("#### Digite o nome do livro:               ####");
		livro.setNome(ler.next());
		System.out.println("#### Digite o autor do livro:              ####");
		livro.setAutor(ler.next());
		System.out.println("#### Digite a editora do livro:            ####");
		livro.setEditora(ler.next());
		System.out.println("#### Digite o ano do livro:                ####");
		livro.setAno(ler.next());
		System.out.println("#### Digite o numero de exemplares:        ####");
		livro.setTotalExemplares(ler.nextInt());

		return livro;
	}

	public static void exibirSucesso() {
		System.out.println("Livro cadastrado com sucesso!");
	}
}
