package inf.ufpr.br.ps.interfaces;

import inf.ufpr.br.ps.obras.Periodico;

import java.util.Scanner;

public class InterfaceCadastroPeriodico {
	
	public static Periodico exibirTela() {
		Scanner ler = new Scanner(System.in);
		Periodico periodico = new Periodico();

		System.out.println("############ CADASTRO PERIODICO ###############");
		System.out.println("#### Digite o nome do periodico:           ####");
		periodico.setNome(ler.next());
		System.out.println("#### Digite o volume do periodico:         ####");
		periodico.setVolume(ler.next());
		System.out.println("#### Digite  o mes do periodico:           ####");
		periodico.setMes(ler.next());
		System.out.println("#### Digite o ano do periodico:            ####");
		periodico.setAno(ler.next());
		System.out.println("#### Digite o numero de exemplares:        ####");
		periodico.setTotalExemplares(ler.nextInt());
		
		return periodico;
	}
	
	public static void exibirSucesso() {
		System.out.println("Periodico cadastrado com sucesso!");
	}
}
