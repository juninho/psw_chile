package inf.ufpr.br.ps.interfaces;

import inf.ufpr.br.ps.usuarios.Aluno;
import inf.ufpr.br.ps.usuarios.Geral;
import inf.ufpr.br.ps.usuarios.Professor;

import java.util.Scanner;

public class InterfaceCadastroUsuario {
	public static int exibirTela() {
		Scanner ler = new Scanner(System.in);
		
		System.out.println("############ CADASTRO USUARIO #############");
		System.out.println("#### Digite o tipo de usuario          ####");
		System.out.println("#### 1- Professor                      ####");
		System.out.println("#### 2- Aluno                          ####");
		System.out.println("#### 3- Geral                          ####");
		System.out.println("###########################################");
		int tipoUser = ler.nextInt();
		
		return tipoUser;
	}

	public static Professor exibirTelaProfessor() {
		Scanner ler = new Scanner(System.in);
		Professor professor = new Professor();
		
		System.out.println("#### Digite o nome do professor:       ####");
		professor.setNome(ler.next());
		System.out.println("#### Digite o cpf do professor:   ####");
		professor.setCpf(ler.next());
		System.out.println("###########################################");
		
		return professor;
	}

	public static Aluno exibirTelaAluno() {
		Scanner ler = new Scanner(System.in);
		Aluno aluno = new Aluno();
		
		System.out.println("#### Digite o nome do aluno:           ####");
		aluno.setNome(ler.nextLine());
		System.out.println("#### Digite o cpf do aluno:       ####");
		aluno.setCpf(ler.next());
		System.out.println("###########################################");
		
		return aluno;
	}

	public static Geral exibirTelaGeral() {
		Scanner ler = new Scanner(System.in);
		Geral geral = new Geral();
		
		System.out.println("#### Digite o nome do usuario:         ####");
		geral.setNome(ler.next());
		System.out.println("#### Digite o cpf do usuario:     ####");
		geral.setCpf(ler.next());
		System.out.println("###########################################");
		
		return geral;

	}
	
	public static void exibirSucesso() {
		System.out.println("Usuario cadastrado com sucesso!");
	}
}
