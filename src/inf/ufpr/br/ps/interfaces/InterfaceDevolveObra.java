package inf.ufpr.br.ps.interfaces;

import inf.ufpr.br.ps.usuarios.Usuario;

import java.util.Scanner;

public class InterfaceDevolveObra {
	public static String exibirTelaUsuario() {
		Scanner ler = new Scanner(System.in);
		System.out.println("#### Digite o cpf do usuario:               ####");
		String cpf = ler.next();
		return cpf;
	}

	public static int exibirPendencias(Usuario usuario) {
		if (usuario.getEmprestimos().isEmpty()) {
			System.out.println("Usuario nao possui emprestimos no momento");
			return 0;
		} else {
			for (int i=0; i < usuario.getEmprestimos().size(); i++) {
				System.out.println("####################################################");
				System.out.println("Obra: " + i);
				System.out.println("Nome: " + usuario.getEmprestimos().get(i).getObra().getNome());
				System.out.println("Limite devolucao: " + usuario.getEmprestimos().get(i).getDataLimite());
				System.out.println("####################################################");
			}
			return 1;
		}
	}
	public static int selecionaObra() {
		Scanner ler = new Scanner(System.in);
		System.out.println("#### Insira o numero de qual obra deseja devolver: ####");
		int obra = ler.nextInt();
		return obra;
	}

	public static String dataDevolucao() {
		Scanner ler = new Scanner(System.in);
		System.out.println("#### Digite a data da devolucao:                  ####");
		String dataDevolucao = ler.next();
		return dataDevolucao;
	}

	public static int pagarMulta(int multa) {
		Scanner ler = new Scanner(System.in);
		System.out.println("#### Deseja pagar a multa de: R$" + multa + ",00? ####");
		System.out.println("0- Sim  1- Nao");
		int obra = ler.nextInt();
		return obra;
	}

	public static void logMultaSucesso() {
		System.out.println("Multa Paga com sucesso!");
	}

	public static void logDevolucaoSucesso() {
		System.out.println("Obra devolvida com sucesso!");
	}

	public static void logMultaNaoPaga() {
		System.out.println("Multa nao foi paga!");
	}

	public static void logDevolucaoNaoFeita() {
		System.out.println("Devolucao nao realizada!");

	}

}
