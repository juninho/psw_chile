package inf.ufpr.br.ps.interfaces;

import java.util.Scanner;

public class InterfaceEmprestaLivro {
	public static String exibirTelaLivro() {
		String nome;
		Scanner ler = new Scanner(System.in);
		System.out.println("############## PESQUISA LIVRO #################");
		System.out.println("#### Digite o nome do livro:               ####");
		nome = ler.next();
		return nome;
	}

	public static String exibirTelaUsuario() {
		String cpf;
		Scanner ler = new Scanner(System.in);
		System.out.println("#### Digite o cpf do usuario:               ####");
		cpf = ler.next();
		return cpf;
	}

	public static String exibirTelaData() {
		String data;
		Scanner ler = new Scanner(System.in);
		System.out.println("#### Digite a data de emprestimo:           ####");
		data = ler.next();
		return data;
	}

	public static void exibirTelaSemLivros() {
		System.out
				.println("Nao ha exemplares do livro disponiveis no momento!");
	}

	public static void exibirTelaLimiteEmprestimos() {
		System.out
				.println("Usuario ja atingiu o limite de emprestimos possiveis!");
	}

	public static void exibirTelaSucesso(String dataLimite) {
		System.out.println("Emprestimo realizado com sucesso!");
		System.out.println("Data para devolucao: " + dataLimite);
	}
}
