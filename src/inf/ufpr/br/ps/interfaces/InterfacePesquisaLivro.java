package inf.ufpr.br.ps.interfaces;

import inf.ufpr.br.ps.obras.Livro;

import java.util.List;
import java.util.Scanner;

public class InterfacePesquisaLivro {
	public static String exibirTela() {
		String nome;
		Scanner ler = new Scanner(System.in);
		System.out.println("############## PESQUISA LIVRO #################");
		System.out.println("#### Digite o nome do livro:               ####");
		nome = ler.next();
		return nome;
	}

	public static int exibirTelaNaoEncontrado() {
		int entrada;
		Scanner ler = new Scanner(System.in);
		System.out.println("####Nenhum Livro com este nome foi encontrado###");
		System.out.println("####     Deseja Fazer nova busca?           ####");
		System.out.println("####           1- Sim || 2- Nao             ####");
		entrada = ler.nextInt();
		return entrada;
	}

	public static void exibirLivro(Livro livro) {
		
			System.out.println("Livro ");
			System.out.print("Nome: ");
			System.out.println(livro.getNome());
			System.out.print("Ano: ");
			System.out.println(livro.getAno());
			System.out.print("Editora: ");
			System.out.println(livro.getEditora());
			System.out.print("Autor: ");
			System.out.println(livro.getAutor());
			System.out.print("Total Exemplares: ");
			System.out.println(livro.getTotalExemplares());
			System.out.print("Exemplares Disponiveis: ");
			System.out.println(livro.getExemplaresDisponiveis());
		
	}
}
