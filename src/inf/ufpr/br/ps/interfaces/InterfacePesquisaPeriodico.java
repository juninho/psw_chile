package inf.ufpr.br.ps.interfaces;

import inf.ufpr.br.ps.obras.Periodico;

import java.util.Scanner;

public class InterfacePesquisaPeriodico {
	public static String exibirTela() {
		String nome;
		Scanner ler = new Scanner(System.in);
		System.out.println("############## PESQUISA PERIODICO #################");
		System.out.println("#### Digite o nome do periodico:               ####");
		nome = ler.next();
		return nome;
	}

	public static int exibirTelaNaoEncontrado() {
		int entrada;
		Scanner ler = new Scanner(System.in);
		System.out.println("Nenhum Periodico com este nome foi encontrado");
		System.out.println("Deseja Fazer nova busca?");
		System.out.println("1- Sim || 2- Nao");
		entrada = ler.nextInt();
		return entrada;
	}

	public static void exibirPeriodico(Periodico periodico) {
			System.out.println("Periodico");
			System.out.print("Nome: ");
			System.out.println(periodico.getNome());
			System.out.print("Ano: ");
			System.out.println(periodico.getAno());
			System.out.print("Mes: ");
			System.out.println(periodico.getMes());
			System.out.print("Volume: ");
			System.out.println(periodico.getVolume());
			System.out.print("Total Exemplares: ");
			System.out.println(periodico.getTotalExemplares());
			System.out.print("Exemplares Disponiveis: ");
			System.out.println(periodico.getExemplaresDisponiveis());
	}
}
