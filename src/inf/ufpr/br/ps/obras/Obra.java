package inf.ufpr.br.ps.obras;

public class Obra {
	protected String nome;
	protected String ano;
	protected int totalExemplares;
	protected int exemplaresDisponiveis; 
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public int getTotalExemplares() {
		return totalExemplares;
	}
	public void setTotalExemplares(int totalExemplares) {
		this.totalExemplares = totalExemplares;
		this.exemplaresDisponiveis = totalExemplares;
	}
	public int getExemplaresDisponiveis() {
		return exemplaresDisponiveis;
	}
	public void setExemplaresDisponiveis(int exemplaresDisponiveis) {
		this.exemplaresDisponiveis = exemplaresDisponiveis;
	}
	
}
