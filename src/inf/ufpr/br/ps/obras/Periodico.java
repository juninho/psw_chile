package inf.ufpr.br.ps.obras;

public class Periodico extends Obra{
	
	private String volume;
	private String mes;
	
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	
}
