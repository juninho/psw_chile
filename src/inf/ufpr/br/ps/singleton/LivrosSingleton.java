package inf.ufpr.br.ps.singleton;

import inf.ufpr.br.ps.dao.LivroDAO;

public class LivrosSingleton {
	private static LivroDAO livros;

	private LivrosSingleton() {
	}

	public static synchronized LivroDAO getInstance() {
		if (livros == null) {
			livros = new LivroDAO();
		}
		return livros;
	}

}
