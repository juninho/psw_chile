package inf.ufpr.br.ps.singleton;

import inf.ufpr.br.ps.dao.PeriodicoDAO;

public class PeriodicosSingleton {
	private static PeriodicoDAO periodicos;

	private PeriodicosSingleton() {
	}

	public static synchronized PeriodicoDAO getInstance() {
		if (periodicos == null) {
			periodicos = new PeriodicoDAO();
		}
		return periodicos;
	}
}
