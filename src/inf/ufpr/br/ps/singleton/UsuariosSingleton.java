package inf.ufpr.br.ps.singleton;

import inf.ufpr.br.ps.dao.UsuarioDAO;

public class UsuariosSingleton {
	private static UsuarioDAO usuarios;

	private UsuariosSingleton() {
	}

	public static synchronized UsuarioDAO getInstance() {
		if (usuarios == null) {
			usuarios = new UsuarioDAO();
		}
		return usuarios;
	}
}
