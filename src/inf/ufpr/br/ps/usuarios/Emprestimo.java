package inf.ufpr.br.ps.usuarios;

import inf.ufpr.br.ps.obras.Obra;
import inf.ufpr.br.ps.util.Util;

public class Emprestimo {
	private String dataEmprestimo;
	private String dataDevolucao;
	private String dataLimite;
	private Obra obra;
	private int multa;

	public Obra getObra() {
		return obra;
	}

	public String getDataEmprestimo() {
		return dataEmprestimo;
	}

	public String getDataLimite() {
		return dataLimite;
	}
	
	public void setDataEmprestimo(String dataEmprestimo, int maxDias) {
		this.dataEmprestimo = dataEmprestimo;
		this.dataLimite = Util.addDia(this.dataEmprestimo, maxDias);
	}

	public String getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(String dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public void setObra(Obra obra) {
		this.obra = obra;
	}

	public int calculaMulta() {
		if (Util.subData(this.dataLimite, this.dataDevolucao) > 0) {
			this.multa = Util.subData(this.dataLimite, this.dataDevolucao) * 5;
		} else
			this.multa = 0;
		return this.multa;
	}

}
