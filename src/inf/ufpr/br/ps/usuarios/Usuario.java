package inf.ufpr.br.ps.usuarios;

import java.util.ArrayList;
import java.util.List;

public class Usuario {

	protected String nome;
	protected String cpf;
	protected int maxEmp;
	protected int maxDias;
	protected List<Emprestimo> emprestimos = new ArrayList<Emprestimo>();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public List<Emprestimo> getEmprestimos() {
		return emprestimos;
	}

	public void novoEmprestimo(Emprestimo emprestimo) {
		this.emprestimos.add(emprestimo);
	}
	
	public int getMaxEmp() {
		return maxEmp;
	}

	public int getMaxDias() {
		return maxDias;
	}
}
