package inf.ufpr.br.ps.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Util {
	public static SimpleDateFormat stringToDate = new SimpleDateFormat(
			"dd/MM/yyyy");

	public static String addDia(String data, int qtd) {
		int ano, mes, dia;
		ano = Integer.parseInt(data.substring(6, 10));
		mes = Integer.parseInt(data.substring(3, 5));
		dia = Integer.parseInt(data.substring(0, 2));
		if (qtd == 30) {
			if (dia == 1)
				dia = dia + 30;
			else {
				dia = dia - 1;
				mes = mes + 1;
			}
		}
		if (qtd == 15) {
			if (dia + 15 > 31) {
				dia = dia + 15 - 31;
				mes = mes + 1;
			} else {
				dia = dia + 15;
			}
		}
		if (qtd == 7) {
			if (dia + 7 > 31) {
				dia = dia + 7 - 31;
				mes = mes + 1;
			} else {
				dia = dia + 7;
			}
		}
		if (mes == 13) {
			mes = 1;
			ano = ano + 1;
		}
		data = "";
		if (dia < 10)
			data = data.concat("0" + Integer.toString(dia));
		else
			data = data.concat(Integer.toString(dia));
		if (mes < 10)
			data = data.concat("/0" + Integer.toString(mes) + "/"
					+ Integer.toString(ano));
		else
			data = data.concat("/" + Integer.toString(mes) + "/"
					+ Integer.toString(ano));
		return data;
	}

	public static int subData(String a, String b) {
		DateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		Date data3 = null;
		Date data4 = null;
		long m1 = 0;
		long m2 = 0;
		try {
			data3 = (Date) fmt.parse(a);
			data4 = (Date) fmt.parse(b);
		} catch (Exception e) {
		}
		Calendar data1 = new GregorianCalendar();
		data1.setTime(data3);
		Calendar data2 = new GregorianCalendar();
		data2.setTime(data4);
		m1 = data1.getTimeInMillis();
		m2 = data2.getTimeInMillis();
		return ((int) ((m2 - m1) / (24 * 60 * 60 * 1000)));
	}
}
